package Tsoft.stepsDefinition;

import Tsoft.objects.oRegistrar;
import Tsoft.steps.sReutilizable;
import Tsoft.steps.otrasAcciones.sAccion;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class dPrueba {

	@Steps
	sReutilizable reutilizable;
	@Steps
	sAccion accionUno;
	
	@Given("^Abrir navegador$")
	public void abrir_navegador() {
		reutilizable.url();	}

	@When("^Ingresar \"([^\"]*)\" y \"([^\"]*)\"$")
	public void ingresar_y(String arg1, String arg2) {
		reutilizable.login(arg1, arg2);	}

	@And("^Navegar por la web$")
	public void navegar_por_la_web() {
		accionUno.anotherThing();	}

	@Then("^Validar logueo correcto$")
	public void validar_logueo_correcto() {
		String obj = oRegistrar.pruebaMensaje.getText();
		reutilizable.closeSession(obj, null);
		String dato = oRegistrar.msjLogOut.getText();
		reutilizable.mensajePantalla(dato);	
		}
}





